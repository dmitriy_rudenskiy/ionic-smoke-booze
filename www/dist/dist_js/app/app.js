angular.module('starter', ['ionic', 'starter.controllers', 'templates']);

angular.module('starter').constant('BASE_URL', 'http://api.zapchasti.site/v1')
angular.module('starter').constant('defaultCenter', {lat: 55.0582120762845, lng: 82.9386973567307})

// Авторизация пользователя
angular.module('starter').factory('AuthService', ['$http', 'BASE_URL', function ($http, BASE_URL) {
    return {
        attempt: function (data) {
            $http({
                method: 'POST',
                url: BASE_URL + '/login',
                data: data
            }).then(function () {

                },
                function () {

                });

        },

        check: function () {
            return true;
        },

        login: function () {

        },
        logout: function () {

        }
    }
}]);

angular.module('starter').run(['$ionicPlatform', '$rootScope', '$state', 'AuthService', function ($ionicPlatform, $rootScope, $state, AuthService) {
    $ionicPlatform.ready(function () {
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }

        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });

    $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams, fromState, fromStateParams) {

        // TODO: отключаем проверку авторизации
        /*
         // проверка авторизации
         if(toState.name != 'app.login' && !AuthService.check()) {
         event.preventDefault();
         $state.go("app.login");
         }

         // пользователь авторизирован
         if(toState.name == 'app.login' && AuthService.check()) {
         event.preventDefault();
         $state.go("app.search");
         }
         */
    });
}])

angular.module('starter').config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    $stateProvider.state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'menu.html',
        controller: 'AppCtrl'
    });

    $stateProvider.state('app.login', {
        url: '/login',
        templateUrl: 'login.html'
    });

    // новости
    $stateProvider.state('app.feed', {
        url: '/feed',
        views: {
            'menuContent': {
                templateUrl: 'feed.html',
                controller: 'FeedCtrl'
            }
        }
    });

    // Список компаний
    $stateProvider.state('app.playlists', {
        url: '/companies',
        views: {
            'menuContent': {
                templateUrl: 'templates/companies.html',
                controller: 'ListsCtrl'
            }
        }
    });

    // Просмотр анкеты компании
    $stateProvider.state('app.company', {
        url: '/company/view/:id',
        views: {
            'menuContent': {
                templateUrl: 'templates/company.html',
                controller: 'ViewCtrl'
            }
        }
    });

    // карта
    $stateProvider.state('app.map', {
        url: '/map',
        views: {
            'menuContent': {
                templateUrl: 'map.html',
                controller: 'MapCtrl'
            }
        }
    });

    $stateProvider.state('app.search', {
        url: '/search',
        views: {
            'menuContent': {
                templateUrl: 'search.html'
            }
        }
    });

    $stateProvider.state('app.browse', {
        url: '/browse',
        views: {
            'menuContent': {
                templateUrl: 'browse.html'
            }
        }
    });

    // просмотр анкеты компании
    $stateProvider.state('app.single', {
        url: '/playlists/:id',
        views: {
            'menuContent': {
                templateUrl: 'playlist.html',
                controller: 'PlaylistCtrl'
            }
        }
    });

    //$urlRouterProvider.otherwise('/app/companies');
}]);


angular.module('starter').directive('map', ['defaultCenter', function (defaultCenter) {
    return {
        restrict: 'E',
        scope: {
            onCreate: '&'
        },
        link: function ($scope, $element, $attr) {
            function initialize() {
                var mapOptions = {
                    center: new google.maps.LatLng(defaultCenter.lat, defaultCenter.lng),
                    zoom: 16,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map($element[0], mapOptions);

                $scope.onCreate({map: map});

                // Stop the side bar from dragging when mousedown/tapdown on the map
                google.maps.event.addDomListener($element[0], 'mousedown', function (e) {
                    e.preventDefault();
                    return false;
                });
            }

            if (document.readyState === "complete") {
                initialize();
            } else {
                google.maps.event.addDomListener(window, 'load', initialize);
            }
        }
    }
}]);

