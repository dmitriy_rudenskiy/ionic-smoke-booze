angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

/*
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
  */
});

angular.module('starter.controllers').controller('ListsCtrl', function($scope, $http) {
    $scope.companies = [];

    $http({
        method: 'GET',
        url: 'http://api.zapchasti.site/v1/companies/1'
    }).then(function successCallback(response) {
        $scope.companies = response.data;


        console.log($scope.companies);
    }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
    });
});

angular.module('starter.controllers').controller('ViewCtrl', function($scope, $stateParams, $ionicHistory, $http) {
    var id = $stateParams.id * 1;

    $scope.company = null;

    $http({
        method: 'GET',
        url: 'http://api.zapchasti.site/v1/company/view/' + id
    }).then(function successCallback(response) {
        $scope.company = response.data;


        console.log($scope.company);
    }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
    });


    console.log('Company ' + id);

    $scope.goBack = function() {
        $ionicHistory.goBack();
    };
});


angular.module('starter.controllers').controller('PlaylistCtrl', function($scope, $stateParams) {
  $scope.id = $stateParams.id * 1;
})

.controller('LoginCtrl', function($scope, $stateParams, AuthService) {
  $scope.model = {
    email: '',
    password: ''
  };

  $scope.doLogin = function() {
    console.log(AuthService.attempt($scope.model));
  };
})

.controller('FeedCtrl', function($scope, $stateParams, $http, $ionicModal, AuthService, FeedService) {

  // Create and load the Modal
  $ionicModal.fromTemplateUrl('new-task.html', function(modal) {
    $scope.taskModal = modal;
  }, {
    scope: $scope,
    animation: 'slide-in-up'
  });

  // Open our new task modal
  $scope.selectCity = function() {
    $scope.taskModal.show();
  };


        $scope.post = [];

        $http({
            method: 'GET',
            url: 'http://api.zapchasti.site/v1/post/1'
        }).then(function successCallback(response) {
            $scope.post = response.data;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
})

 .controller('MapCtrl', function ($scope, $ionicLoading, $compile, defaultCenter, MakerService) {

var i = 1;

            // добавление точек
            function addMarker(lat, lng, title, map, isCenter, address) {
                
                if (!isCenter) {
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(lat, lng),
                        map: map,
                        title: title,
                    });

                    return;
                }
                
                //Marker + infowindow + angularjs compiled ng-click
                var contentString = "<div><h5>" + title + "</h5><p>" + address + "</p><a href='#/app/playlists/" + i + "'>смотреть</a></div>";
                var compiled = $compile(contentString)($scope);

                i = i + 1;

                var infowindow = new google.maps.InfoWindow({
                    content: compiled[0]
                });

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lng),
                    map: map,
                    title: title,
                    icon: {
                        url: 'img/flag.png',
                        anchor: new google.maps.Point(0, 0),
                        scaledSize: new google.maps.Size(64, 64)
                    }
                });

                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(map, marker);
                });
            }

            $scope.mapCreated = function (map) {
                $scope.map = map;

                navigator.geolocation.getCurrentPosition(function (pos) {
                    $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
                    addMarker(pos.coords.latitude, pos.coords.longitude, "Я тут", $scope.map, false);
                });
                
                // добавляем маркеры на карту
                angular.forEach(MakerService.getList(), function (value) {
                    addMarker(value.getLat(), value.getLng(), value.getTitle(), $scope.map, true, value.getAddress());
                });
            };

            /**
             * Set center
             */
            $scope.centerOnMe = function () {

                if (!$scope.map) {
                    return;
                }

                $scope.loading = $ionicLoading.show({
                    content: 'Getting current location...',
                    showBackdrop: false
                });

                navigator.geolocation.getCurrentPosition(function (pos) {
                    //console.log('Got pos', pos);
                    $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
                    $scope.loading.hide();
                }, function (error) {
                    alert('Unable to get location: ' + error.message);
                });
            };
})

      .factory('Article', function () {

            function Article(title, content, image, url) {
                this.title = title;
                this.content = content;
                this.image = image;
                this.url = url;

                return this;
            }

            Article.prototype = {
                getTitle: function () {
                    return this.title;
                },
                getContent: function () {
                    return this.content;
                },
                getImage: function () {
                    return this.image;
                },
                getUrl: function () {
                    return this.url;
                }
            };

            return Article;
        })

        .factory('FeedService', function (Article) {
            return  {
                getList: function () {

                    var list = [];

                    list.push(new Article('', 'Добрые и милые иллюстрации', 'http://4tololo.ru/files/styles/large/public/images/20121404235831.jpg'));
                    list.push(new Article('', 'Самые добрые фотографии объятий животных', 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSE8oFerfl9SwpPcfaUAXWHNwriYMvAPNIubw32YoTiQ2dmba2EHw'));

                    return list;
                }
            };
        })
        .factory('Maker', function () {

            function Maker() {
                this.id = 0;
                this.title = '';
                this.address = '';
                this.lat = 0.0;
                this.lng = 0.0;

                return this;
            }

            Maker.prototype = {
                init: function (obj) {
                    this.title = obj.title;
                    this.address = obj.address;
                    this.lat = obj.lat;
                    this.lng = obj.lng;

                    return this;
                },
                getId: function () {
                    return this.id;
                },
                getTitle: function () {
                    return this.title;
                },
                getAddress: function () {
                    console.log(this.address);
                    return this.address;
                },
                getLat: function () {
                    return this.lat;
                },
                getLng: function () {
                    return this.lng;
                }
            };

            return Maker;
        })

        .factory('MakerService', function (Maker) {

            function getList (lat, lng) {
                var list = [];

                var data = JSON.parse('[{"title":"Ресторан «Хороший год»","address":"Красный проспект 12","lat":55.018938,"lng":82.923852},{"title":"Бар «Vorobey»","address":"Красный проспект 67","lat":55.043598,"lng":82.916217},{"title":"Бар «Trend»","address":"Красный проспект 67","lat":55.043598,"lng":82.916217},{"title":"Магазин «Табакерия»","address":"Октябрьская, 33","lat":55.024698,"lng":82.918759},{"title":"Магазин «Табакерия»","address":"Горский микрорайон, 82","lat":54.994336,"lng":82.892788},{"title":"Магазин «Табакерия»","address":"Ипподромская, 46","lat":55.059198,"lng":82.938917},{"title":"Универсам «На Ленина»","address":"Ленина, 10","lat":55.030016,"lng":82.91438},{"title":"Black Milk","address":"Ядринцевская, 21","lat":55.033171,"lng":82.920816},{"title":"Паб «Дублин»","address":"Максима Горького, 54","lat":55.026876,"lng":82.919468}]');

                angular.forEach(data, function (value) {
                    list.push(new Maker().init(value));
                });

                return list;
            }
            
            return {
                getList: getList
            };
        });
